import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'app/main/auth/auth.service';
import { User } from 'app/models/user.model';
import { UsersService } from 'app/services/users.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  activeUser: User | null = null;

  isAdmin: boolean = false;

  subs!: Subscription;

  lang!: string;

  constructor(
    public userService: UsersService,
    private router: Router,
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    const usersLocale = this.getUsersLocale('en');

    if (usersLocale !== 'ru-RU' || 'ru') {
      this.lang = localStorage.getItem('lang') || 'en';
    } else {
      localStorage.getItem('lang') || 'ru';
    }

    this.isAdmin = this.userService.isAdmin();
    this.activeUser = this.userService.getUserFromStorage();

    this.subs = this.userService.authStateChange
      .subscribe(
        () => {
          this.isAdmin = this.userService.isAdmin();
          this.activeUser = this.userService.getUserFromStorage();
        },
      );
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  onSignOut(): void {
    this.activeUser = null;
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  changeLang(event: Event) {
    const lang = (event.target as HTMLInputElement).value;

    localStorage.setItem('lang', lang);
    window.location.reload();
  }

  getUsersLocale(defaultValue: string): string {
    if (typeof window === 'undefined' || typeof window.navigator === 'undefined') {
      return defaultValue;
    }

    const wn = window.navigator as any;
    let lang = wn.languages ? wn.languages[0] : defaultValue;
    lang = lang || wn.language || wn.browserLanguage || wn.userLanguage;

    return lang;
  }
}
