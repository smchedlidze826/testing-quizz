import {
  AfterViewInit,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Quizz } from 'app/models/quizz.model';
import { User } from 'app/models/user.model';

const tableHeader: string[] = ['title', 'category', 'score', 'time', 'difficulty'];

@Component({
  selector: 'app-profile-table',
  templateUrl: './profile-table.component.html',
  styleUrls: ['./profile-table.component.scss'],
})
export class ProfileTableComponent implements AfterViewInit, OnInit {
  displayedColumns: string[] = tableHeader;

  titleFilter: string = 'title';

  difficultyFilter: string = 'difficulty';

  categoryFilter: string = 'category';

  selected: string = this.titleFilter;

  dataSource!: MatTableDataSource<Quizz>;

  pageSizeOptions: number[] = [5];

  @Input() activeUser!: User;

  @Input() passedTests!: Quizz[];

  @ViewChild(MatPaginator, { static: false }) paginator!: MatPaginator;

  @ViewChild(MatSort, { static: false }) sort!: MatSort;

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource(this.passedTests);
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event, filterCategory: string) {
    switch (filterCategory) {
      case this.categoryFilter:
        this.dataSource.filterPredicate = (data, filter: string) => data.category.toLowerCase().includes(filter);
        break;

      case this.difficultyFilter:
        this.dataSource.filterPredicate = (data, filter: string) => data.difficulty.toLowerCase().includes(filter);
        break;

      default: this.dataSource.filterPredicate = (data, filter: string) => data.title.toLowerCase().includes(filter);
    }

    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
