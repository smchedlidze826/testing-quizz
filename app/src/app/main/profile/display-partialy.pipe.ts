import { Pipe, PipeTransform } from '@angular/core';
import { Quizz } from 'app/models/quizz.model';

const DEFAULT_DISPLAY_PARTIALY_LENGTH = 4;

@Pipe({
  name: 'displayPartialy',
})

export class DisplayPartialyPipe implements PipeTransform {
  transform(arr: Quizz[], size: number = DEFAULT_DISPLAY_PARTIALY_LENGTH): Quizz[] {
    const newArr = arr.slice(0, size);

    return newArr;
  }
}
