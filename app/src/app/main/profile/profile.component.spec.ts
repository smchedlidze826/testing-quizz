import { MatDialog } from '@angular/material/dialog';
import { UsersService } from 'app/services/users.service';
import { ProfileComponent } from './profile.component';

describe('Profile dialog', () => {
  let component: ProfileComponent;
  let service: UsersService;
  let dialog: MatDialog;
  beforeEach(() => { component = new ProfileComponent(service, dialog); });

  it('should be called once', () => {
    spyOn(component, 'openEditMode');
    expect(component.openEditMode).toHaveBeenCalledTimes(0);
  });

  it('should be null or undefined', () => {
    expect(component.displayPartialy).toBeTrue();
  });
});
