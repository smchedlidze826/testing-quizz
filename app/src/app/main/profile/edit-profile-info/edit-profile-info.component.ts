import {
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from 'app/models/user.model';
import { UsersService } from 'app/services/users.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-edit-profile-info',
  templateUrl: './edit-profile-info.component.html',
  styleUrls: ['./edit-profile-info.component.scss'],
})
export class EditProfileInfoComponent implements OnInit {
  today = new Date();

  user!: User;

  @ViewChild('form', { static: false }) form!: NgForm;

  constructor(
    private userService: UsersService,
    public dialogRef: MatDialogRef<EditProfileInfoComponent>,
    @Inject(MAT_DIALOG_DATA) public userData: User,
  ) { }

  ngOnInit(): void {
    this.user = this.userData;
  }

  onClose(): void {
    this.dialogRef.close(this.user);
  }

  onSave():void {
    this.userService.update(this.user);

    this.onClose();
  }
}
