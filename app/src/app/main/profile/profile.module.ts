import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatDividerModule } from '@angular/material/divider';

import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { ProfileTableComponent } from './profile-table/profile-table.component';
import { ProfileRoutingModule } from './profile-routing.module';
import { EditProfileInfoComponent } from './edit-profile-info/edit-profile-info.component';
import { ProfileComponent } from './profile.component';
import { DisplayPartialyPipe } from './display-partialy.pipe';

@NgModule({
  declarations: [
    ProfileComponent,
    EditProfileInfoComponent,
    ProfileTableComponent,
    DisplayPartialyPipe,
  ],
  imports: [
    FormsModule,
    CommonModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatChipsModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDividerModule,
    ProfileRoutingModule,
  ],
})

export class ProfileModule {}
