import { Component, OnInit } from '@angular/core';
import { User } from 'app/models/user.model';
import { UsersService } from 'app/services/users.service';

import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

import { MatDialog } from '@angular/material/dialog';
import { EditProfileInfoComponent } from './edit-profile-info/edit-profile-info.component';

@UntilDestroy()
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  activeUser!: User;

  displayPartialy: boolean = true;

  activeUserPic: string = 'https://i.stack.imgur.com/l60Hf.png';

  constructor(
    private userService: UsersService,
    private dialogRef: MatDialog,
  ) { }

  ngOnInit(): void {
    const user = this.userService.getUserFromStorage();
    if (user) {
      this.activeUser = user;
    }
  }

  openEditMode(): void {
    const dialog = this.dialogRef.open(EditProfileInfoComponent, {
      data: this.activeUser,
      id: 'custom-dialog-container-profile',
    });

    dialog.afterClosed()
      .pipe(untilDestroyed(this))
      .subscribe(
        (data) => {
          this.userService.update(data);
          this.activeUser = data;
          this.userService.updateDataInSessionStorage(this.activeUser);
        },
      );
  }
}
