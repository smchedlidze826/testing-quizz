import {
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Quizz } from 'app/models/quizz.model';
import { DialogMode, QuizzService } from 'app/services/quizz.service';

interface DialogData {
  quizz: Quizz,
  mode: DialogMode
}

@Component({
  selector: 'app-quizz-dialog',
  templateUrl: './quizz-dialog.component.html',
  styleUrls: ['./quizz-dialog.component.scss'],
})
export class QuizzDialogComponent implements OnInit {
  @ViewChild('form', { static: false }) form!: NgForm;

  difficulty: string[] = ['easy', 'medium', 'hard'];

  category: string[] = ['math', 'geography', 'history'];

  quizz!: Quizz;

  mode!: DialogMode;

  ADD_MODE = DialogMode.ADD;

  constructor(
    private quizzService: QuizzService,
    public dialogRef: MatDialogRef<QuizzDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) { }

  ngOnInit(): void {
    this.quizz = this.data.quizz;
    this.mode = this.data.mode;
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onSave(): void {
    let numberOfQuestions = 0;
    if (this.quizz.questions) {
      numberOfQuestions = this.quizz.questions.length;
    }

    this.mode === this.ADD_MODE ? this.quizzService.add({ ...this.quizz, numberOfQuestions })
      : this.quizzService.edit({ ...this.quizz });
    this.onClose();
  }
}
