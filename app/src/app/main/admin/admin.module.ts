import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatOptionModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from '../auth/admin.guard.service';
import { AuthGuard } from '../auth/auth.guard.service';
import { TestsModule } from '../tests/tests.module';
import { AdminComponent } from './admin.component';
import { QuizzDialogComponent } from './quizz-dialog/quizz-dialog.component';

const routes:Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AuthGuard, AdminGuard],
  },
];

@NgModule({
  declarations: [
    AdminComponent,
    QuizzDialogComponent,
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatOptionModule,
    MatSelectModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    TestsModule,
    RouterModule.forChild(routes),
  ],
})

export class AdminModule {}
