import {
  Component, OnDestroy, OnInit, ViewChild,
} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { User } from 'app/models/user.model';
import { UsersService } from 'app/services/users.service';
import { Subscription } from 'rxjs';

const tableHeader = ['name', 'surname', 'score', 'status'];

let limit: number = 2;

@Component({
  selector: 'app-user-score-stats',
  templateUrl: './user-score-stats.component.html',
  styleUrls: ['./user-score-stats.component.scss'],
})
export class UserScoreStatsComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = tableHeader;

  dataSource!: MatTableDataSource<User>;

  nameFilter: string = 'name';

  surnameFilter: string = 'surname';

  statusFilter: string = 'status';

  selected: string = this.nameFilter;

  usersArr!: User[];

  subs!: Subscription;

  showBtn: boolean = true;

  @ViewChild(MatSort, { static: false }) sort!: MatSort;

  constructor(private userService: UsersService) {}

  ngOnInit(): void {
    this.userService.getUsers(limit);

    this.subs = this.userService.usersArr.subscribe((resData) => {
      this.usersArr = resData;
      this.dataSource = new MatTableDataSource(this.usersArr);
      this.set();
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  onNext() {
    limit += 2;
    this.userService.getUsers(limit);
  }

  set(): void {
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event, filterCategory: string) {
    this.showBtn = false;
    const filterValue = (event.target as HTMLInputElement).value;
    this.userService.filterUsers(filterCategory, filterValue, limit);
    if (filterValue === '') {
      this.showBtn = true;
    }
  }
}
