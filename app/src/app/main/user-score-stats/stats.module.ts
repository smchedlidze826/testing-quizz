import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard.service';
import { UserScoreStatsComponent } from './user-score-stats.component';

const routes: Routes = [
  {
    path: '',
    component: UserScoreStatsComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  declarations: [
    UserScoreStatsComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatTableModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatInputModule,
    MatSortModule,
    MatButtonModule,
    RouterModule.forChild(routes),
  ],
})

export class StatsModule { }
