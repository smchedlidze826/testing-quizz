import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard.service';
import { TestsModule } from '../tests/tests.module';
import { TrainingsComponent } from './trainings.component';

const routes:Routes = [
  {
    path: '',
    component: TrainingsComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  declarations: [
    TrainingsComponent,
  ],
  imports: [
    TestsModule,
    RouterModule.forChild(routes),
  ],

})

export class TrainingsModule {}
