import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Quizz } from 'app/models/quizz.model';
import { User } from 'app/models/user.model';
import { QuizzService } from 'app/services/quizz.service';
import { UsersService } from 'app/services/users.service';
import { Subscription } from 'rxjs';
import { TestsService } from '../../../services/tests.service';

@Component({
  selector: 'app-quizz',
  templateUrl: './quizz.component.html',
  styleUrls: ['./quizz.component.scss'],
})
export class QuizzComponent implements OnInit, OnDestroy {
  showResult: boolean = false;

  score: number = 0;

  subs!: Subscription;

  quizz!: Quizz;

  time!: string;

  activeUser!:User;

  constructor(
    private quizzService: QuizzService,
    private route: ActivatedRoute,
    private testsService:TestsService,
    private userService:UsersService,
  ) { }

  ngOnInit(): void {
    const user = this.userService.getUserFromStorage();
    if (user) {
      this.activeUser = user;
    }

    const paramsId = this.route.snapshot.params.id;

    this.startTimer();

    this.quizzService.getQuizzById(paramsId);

    this.subs = this.quizzService.quizzStateChange
      .subscribe(
        (quizz) => {
          if (quizz) {
            this.quizz = quizz;
          }
        },
      );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  increaseScore(score: number): void {
    this.score += score;
  }

  startTimer() {
    let minutes: number = 0;
    let seconds: number = 0;

    const interval = setInterval(() => {
      seconds += 1;

      if (this.showResult) {
        clearInterval(interval);
      }

      if (seconds === 60) {
        seconds = 0;
        minutes += 1;
      }
      this.time = `${minutes}:${seconds}`;

      this.isTestTimeOver(minutes);
    }, 1000);
  }

  isTestTimeOver(minutes:number) {
    if (+this.quizz.time === minutes) {
      this.showResult = true;
      this.testsService.finalize(this.activeUser, this.quizz, this.score, this.time);
    }
  }
}
