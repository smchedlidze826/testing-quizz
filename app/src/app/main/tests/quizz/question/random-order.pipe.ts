import { Pipe, PipeTransform } from '@angular/core';
import { Answer } from 'app/models/answer.model';

@Pipe({ name: 'randomOrder' })
export class RandomOrderPipe implements PipeTransform {
  transform(list: Array<Answer>): Array<Answer> {
    const newList = [...list];
    newList.sort(() => Math.random() - 0.5);
    return newList;
  }
}
