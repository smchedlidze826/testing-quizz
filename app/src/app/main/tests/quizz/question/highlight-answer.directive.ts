import {
  Directive,
  ElementRef,
  HostListener,
  Input,
  Renderer2,
} from '@angular/core';

@Directive({
  selector: '[appHighlightAnswer]',
})
export class HighlightAnswerDirective {
  @Input() appHighlightAnswer!:boolean;

  @Input() answerAccepted:boolean = false;

  class!:string;

  constructor(
    private elRef:ElementRef,
    private renderer:Renderer2,
  ) {
  }

  @HostListener('click') onClick() {
    if (!this.answerAccepted) {
      this.appHighlightAnswer ? this.class = 'correct' : this.class = 'incorrect';
      this.renderer.addClass(this.elRef.nativeElement, this.class);
    }
  }
}
