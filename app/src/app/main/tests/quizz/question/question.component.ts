import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { Question } from 'app/models/questions.model';
import { Quizz } from 'app/models/quizz.model';
import { User } from 'app/models/user.model';
import { UsersService } from 'app/services/users.service';
import { TestsService } from '../../../../services/tests.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss'],
})
export class QuestionComponent implements OnInit {
  @Output() scoreEmitter: EventEmitter<number> = new EventEmitter();

  @Output() showResult: EventEmitter<boolean> = new EventEmitter();

  @Input() quizz!: Quizz;

  @Input() score!: number;

  @Input() time!: string;

  answerAccepted!: boolean;

  randomPosition!: number;

  currentQuestion!: Question;

  currentQuestionIndex: number = 0;

  activeUser!: User;

  passedTests!: Quizz[];

  constructor(
    private userService: UsersService,
    private testsService:TestsService,
  ) { }

  ngOnInit(): void {
    this.answerAccepted = false;

    this.currentQuestion = this.quizz.questions[this.currentQuestionIndex];

    this.randomPosition = Math.round(Math.random() * this.quizz.questions.length);

    const user = this.userService.getUserFromStorage();
    if (user) {
      this.activeUser = user;
    }
  }

  selectAnswer(correct: boolean): void {
    if (!this.answerAccepted && correct) {
      this.scoreEmitter.emit(1);
    }
    this.answerAccepted = true;
  }

  next(): void {
    if (this.quizz.questions.length - 1 !== this.currentQuestionIndex) {
      this.currentQuestionIndex += 1;
      this.currentQuestion = this.quizz.questions[this.currentQuestionIndex];
    } else {
      this.showResult.emit(true);
      this.testsService.finalize(this.activeUser, this.quizz, this.score, this.time);
    }

    this.randomPosition = Math.round(Math.random() * this.quizz.questions.length);

    this.answerAccepted = false;
  }
}
