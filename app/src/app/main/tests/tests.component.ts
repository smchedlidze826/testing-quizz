import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Quizz } from 'app/models/quizz.model';
import { User } from 'app/models/user.model';
import { DialogMode, QuizzService } from 'app/services/quizz.service';
import { UsersService } from 'app/services/users.service';
import { Subscription } from 'rxjs';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { QuizzDialogComponent } from '../admin/quizz-dialog/quizz-dialog.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';

@UntilDestroy()
@Component({
  selector: 'app-tests',
  templateUrl: './tests.component.html',
  styleUrls: ['./tests.component.scss'],
})

export class TestsComponent implements OnInit, OnDestroy {
  quizzArr!: Quizz[];

  activeUser!: User;

  subs!: Subscription;

  isAdmin!: boolean;

  constructor(
    private quizzService: QuizzService,
    private userService: UsersService,
    public router: Router,
    private route: ActivatedRoute,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    const data = this.userService.getUserFromStorage();
    if (data) {
      this.activeUser = data;
    }

    this.quizzService.getQuizz();

    this.isAdmin = this.userService.isAdmin();

    this.subs = this.quizzService.quizzArrStateChange
      .subscribe(
        (responseData) => this.quizzArr = responseData,
      );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  onOpen(quizz: Quizz): void {
    this.router.navigate(['/quizz-info', quizz.id], { relativeTo: this.route });
  }

  onStart(quizz: Quizz): void {
    this.router.navigate(['/quizz', quizz.id], { relativeTo: this.route });
  }

  onEdit(quizz: Quizz): void {
    this.dialog.open(QuizzDialogComponent, {
      data: { quizz, mode: DialogMode.EDIT },
      id: 'custom-dialog-container-admin',
    });

    this.dialog.afterAllClosed
      .pipe(untilDestroyed(this))
      .subscribe(
        () => this.quizzService.getQuizz(),
      );
  }

  deleteCheck(quizz: Quizz) {
    const dialog = this.dialog.open(ConfirmationDialogComponent, {
      data: 'Test',
    });

    dialog.afterClosed()
      .pipe(untilDestroyed(this))
      .subscribe(
        (confirmed: boolean) => {
          if (confirmed) {
            this.onDelete(quizz);
          }
        },
      );
  }

  onDelete(quizz: Quizz): void {
    this.quizzService.delete(quizz.id);
  }

  add(): void {
    const test = this.quizzService.create(`${this.activeUser.name} ${this.activeUser.surname}`);

    this.dialog.open(QuizzDialogComponent, {
      data: { quizz: test, mode: DialogMode.ADD },
      id: 'custom-dialog-container-admin',
    });

    this.dialog.afterAllClosed
      .pipe(untilDestroyed(this))
      .subscribe(
        () => this.quizzService.getQuizz(),
      );
  }
}
