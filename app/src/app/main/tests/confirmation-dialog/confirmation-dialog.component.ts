import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss'],
})
export class ConfirmationDialogComponent implements OnInit {
  dialogData!: string;

  confirm!: boolean;

  constructor(
    public dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string,
  ) { }

  ngOnInit(): void {
    this.dialogData = this.data;
  }

  onConfirm() {
    this.confirm = true;
    this.onClose(this.confirm);
  }

  onClose(isConfirm: boolean) {
    this.dialogRef.close(isConfirm);
  }

  onCancel() {
    this.confirm = false;
    this.onClose(this.confirm);
  }
}
