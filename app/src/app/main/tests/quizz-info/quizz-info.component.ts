import {
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { QuizzDialogComponent } from 'app/main/admin/quizz-dialog/quizz-dialog.component';
import { Question } from 'app/models/questions.model';
import { Quizz } from 'app/models/quizz.model';
import { User } from 'app/models/user.model';
import { DialogMode, QuizzService } from 'app/services/quizz.service';
import { UsersService } from 'app/services/users.service';
import { Subscription } from 'rxjs';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { CreateQuizzComponent } from './create-quizz/create-quizz.component';

@UntilDestroy()
@Component({
  selector: 'app-quizz-info',
  templateUrl: './quizz-info.component.html',
  styleUrls: ['./quizz-info.component.scss'],
})
export class QuizzInfoComponent implements OnInit, OnDestroy {
  quizz!: Quizz;

  questions!: Question[];

  ADMIN_STATUS: string = 'admin';

  activeUser: User | null = null;

  subs!: Subscription;

  constructor(
    private quizzService: QuizzService,
    private route: ActivatedRoute,
    private userService: UsersService,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    const selectedQuizzId = this.route.snapshot.params.id;

    this.quizzService.getQuizzById(selectedQuizzId);

    this.subs = this.quizzService.quizzStateChange
      .subscribe(
        (quizz) => {
          if (quizz) {
            this.quizz = quizz;
            this.questions = quizz.questions;
          }
        },
      );

    this.activeUser = this.userService.getUserFromStorage();
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  openEditMode(): void {
    this.dialog.open(QuizzDialogComponent, {
      data: { quizz: this.quizz, mode: DialogMode.EDIT },
      id: 'custom-dialog-container-info',
    });
  }

  openTaskDialog(): void {
    const dialog = this.dialog.open(CreateQuizzComponent, {
      id: 'custom-dialog-container-create-task',
    });

    dialog.afterClosed()
      .pipe(untilDestroyed(this))
      .subscribe((questionData: Question) => {
        if (questionData && this.questions) {
          this.questions.push(questionData);
          this.quizz = { ...this.quizz, questions: this.questions, numberOfQuestions: this.quizz.questions.length } as Quizz;
          this.quizzService.quizzStateChange.next(this.quizz);
        }

        if (questionData && !this.questions) {
          this.quizz = { ...this.quizz, questions: [questionData], numberOfQuestions: 1 } as Quizz;
          this.quizzService.quizzStateChange.next(this.quizz);
        }

        this.quizzService.edit(this.quizz);
      });
  }
}
