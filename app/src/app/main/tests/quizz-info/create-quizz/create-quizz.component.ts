import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
} from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Answer } from 'app/models/answer.model';
import { Question } from 'app/models/questions.model';
import { QuizzService } from 'app/services/quizz.service';

@Component({
  selector: 'app-create-quizz',
  templateUrl: './create-quizz.component.html',
  styleUrls: ['./create-quizz.component.scss'],
})
export class CreateQuizzComponent implements OnInit, AfterViewInit {
  taskForm!: FormGroup;

  checkboxRequired:boolean = true;

  answersDoNotMatch:boolean = true;

  errorMsg!:string;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<CreateQuizzComponent>,
    private cdr: ChangeDetectorRef,
    private quizzService: QuizzService,
    @Inject(MAT_DIALOG_DATA) public data?: Question,

  ) { }

  ngOnInit(): void {
    if (this.data) {
      this.taskForm = this.create(this.data.answerList.length);
      this.taskForm.patchValue(this.data);
      this.checkboxRequired = false;
    } else {
      this.taskForm = this.create();
    }
  }

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }

  getAnswerModel() {
    return this.fb.group({
      correct: this.fb.control(false),
      answerText: this.fb.control('', [
        Validators.required,
        Validators.minLength(3),
      ]),
    });
  }

  create(numberOfQuestions: number = 2): FormGroup {
    return this.fb.group({
      question: this.fb.control('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      answerList: this.fb.array(Array.from({ length: numberOfQuestions },
        () => this.getAnswerModel())),
    });
  }

  get answers() {
    return this.taskForm.controls.answerList as FormArray;
  }

  add(): void {
    this.answers.push(
      this.fb.group({
        correct: this.fb.control(false),
        answerText: this.fb.control('', [
          Validators.required,
          Validators.minLength(3),
        ]),
      }),
    );
  }

  remove(index: number): void {
    this.answers.removeAt(index);
    this.isCheckboxRequired();
    this.cdr.detectChanges();
  }

  isCheckboxRequired(): boolean {
    this.checkboxRequired = !this.taskForm.controls.answerList.value.find((answer: Answer) => answer.correct);
    this.cdr.detectChanges();
    return this.checkboxRequired;
  }

  onSubmit() {
    this.answersAreValid();

    if (this.answersDoNotMatch) {
      if (!this.data) {
        const id = this.quizzService.generateId(5);

        this.dialogRef.close({ ...this.taskForm.value, id });
      } else {
        this.dialogRef.close({ ...this.taskForm.value, id: this.data.id });
      }
    } else {
      this.errorMsg = 'No dublicate answers are allowed';
    }
  }

  answersAreValid() {
    const answers = this.taskForm.value.answerList;
    const tmpArr:string[] = [];

    answers.forEach((element:Answer) => {
      if (tmpArr.indexOf(element.answerText) === -1) {
        tmpArr.push(element.answerText);
      }
    });

    this.answersDoNotMatch = tmpArr.length === answers.length;
  }

  onClose() {
    return this.dialogRef.close();
  }
}
