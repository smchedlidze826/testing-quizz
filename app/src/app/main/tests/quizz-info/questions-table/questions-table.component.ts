import {
  Component, Input, OnDestroy, OnInit,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Question } from 'app/models/questions.model';
import { Quizz } from 'app/models/quizz.model';
import { QuizzService } from 'app/services/quizz.service';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

import { ConfirmationDialogComponent } from '../../confirmation-dialog/confirmation-dialog.component';
import { CreateQuizzComponent } from '../create-quizz/create-quizz.component';

const tableHeader: string[] = ['question', 'edit', 'remove'];

@UntilDestroy()
@Component({
  selector: 'app-questions-table',
  templateUrl: './questions-table.component.html',
  styleUrls: ['./questions-table.component.scss'],
})
export class QuestionsTableComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = tableHeader;

  dataSource!: MatTableDataSource<Question>;

  pageSizeOptions: number[] = [5];

  subs!: Subscription;

  dialogSubs!: Subscription;

  confDialogSubs!: Subscription;

  questions!: Question[];

  show!: boolean;

  @Input() quizz!: Quizz;

  constructor(
    private quizzService: QuizzService,
    private dialogRef: MatDialog,
  ) {}

  ngOnInit(): void {
    this.show = this.quizz.questions.length > 0;
    this.questions = this.quizz.questions;
    this.dataSource = new MatTableDataSource(this.questions);

    this.subs = this.quizzService.quizzStateChange.subscribe((quizz) => {
      if (quizz?.questions) {
        this.questions = quizz.questions;
        this.dataSource = new MatTableDataSource(this.questions);
        this.show = quizz.questions.length > 0;
      }
    });
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  deleteCheck(question: Question) {
    const dialog = this.dialogRef.open(ConfirmationDialogComponent, {
      data: 'Question',
    });

    this.confDialogSubs = dialog
      .afterClosed()
      .pipe(
        untilDestroyed(this),
        filter((confirmed: boolean) => confirmed),
      )
      .subscribe(() => this.delete(question));
  }

  delete(question: Question) {
    const updatedQuestArr = this.questions.filter(
      (el) => el.id !== question.id,
    );

    this.quizz = {
      ...this.quizz,
      questions: updatedQuestArr,
      numberOfQuestions: this.quizz.questions.length - 1,
    };

    this.quizzService.edit(this.quizz);
    this.quizzService.quizzStateChange.next(this.quizz);
  }

  onEdit(question: Question) {
    const dialog = this.dialogRef.open(CreateQuizzComponent, {
      data: question,
      id: 'custom-dialog-container-quizz-edit-mode',
    });

    this.dialogSubs = dialog
      .afterClosed()
      .pipe(
        untilDestroyed(this),
        filter((questionsData) => questionsData),
      )
      .subscribe((questionData: Question) => {
        let questionsArr = this.questions;

        questionsArr = this.questions.filter((el) => questionData.id !== el.id);
        questionsArr.push(questionData);

        this.quizz = { ...this.quizz, questions: questionsArr };
        this.quizzService.quizzStateChange.next(this.quizz);

        this.quizzService.edit(this.quizz);
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
