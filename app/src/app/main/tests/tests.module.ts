import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';

import { TranslateModule } from '@ngx-translate/core';
import { QuizzInfoComponent } from './quizz-info/quizz-info.component';
import { HighlightAnswerDirective } from './quizz/question/highlight-answer.directive';
import { QuestionComponent } from './quizz/question/question.component';
import { RandomOrderPipe } from './quizz/question/random-order.pipe';
import { QuizzComponent } from './quizz/quizz.component';
import { ResultComponent } from './quizz/result/result.component';
import { TestsRoutingModule } from './tests-routing.module';
import { TestsComponent } from './tests.component';
import { CreateQuizzComponent } from './quizz-info/create-quizz/create-quizz.component';
import { QuestionsTableComponent } from './quizz-info/questions-table/questions-table.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';

@NgModule({
  declarations: [
    TestsComponent,
    QuizzComponent,
    QuizzInfoComponent,
    ResultComponent,
    QuestionComponent,
    CreateQuizzComponent,
    RandomOrderPipe,
    HighlightAnswerDirective,
    QuestionsTableComponent,
    ConfirmationDialogComponent,
  ],
  imports: [
    ReactiveFormsModule,
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatDialogModule,
    MatSelectModule,
    MatCheckboxModule,
    TestsRoutingModule,
    MatTableModule,
    MatProgressSpinnerModule,
    TranslateModule,
  ],
  exports: [
    TestsComponent,
    QuizzComponent,
    QuizzInfoComponent,
    ResultComponent,
    QuestionComponent,
    CreateQuizzComponent,
    RandomOrderPipe,
    HighlightAnswerDirective,
    TranslateModule,
  ],
})

export class TestsModule {}
