import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuizzInfoComponent } from './quizz-info/quizz-info.component';
import { QuizzComponent } from './quizz/quizz.component';

const routes:Routes = [
  { path: 'quizz/:id', component: QuizzComponent },
  { path: 'quizz-info/:id', component: QuizzInfoComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class TestsRoutingModule {}
