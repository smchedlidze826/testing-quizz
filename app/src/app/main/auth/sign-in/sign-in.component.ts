import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'app/models/user.model';
import { UsersService } from 'app/services/users.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})
export class SignInComponent implements OnInit, OnDestroy {
  subs: Subscription = new Subscription();

  activeUser: User | null = null;

  activeUserId!: string;

  email!: string;

  password!: string;

  errorSubs!: Subscription;

  errorMsg!: string;

  constructor(
    private userService: UsersService,
    private router: Router,
    private authService: AuthService,
  ) {}

  ngOnInit(): void {
    this.errorSubs = this.authService.errorMsgValue.subscribe((errMsg) => {
      this.errorMsg = errMsg;
    });

    this.subs = this.userService.authStateChange.subscribe((userData) => {
      this.activeUser = userData;
      this.router.navigate(['/trainings']);
    });
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
    this.errorSubs.unsubscribe();
  }

  onSubmit(): void {
    this.login(this.email, this.password);
  }

  login(email: string, password: string): void {
    this.authService.login(email, password);
    this.authService.user.subscribe((user) => {
      if (user) {
        this.userService.getActiveUser(user.email);
      }
    });
  }
}
