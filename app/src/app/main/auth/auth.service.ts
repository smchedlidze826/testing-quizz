import { Subject, Subscription } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthResponseData } from 'app/models/auth-response-data.model';
import { UserAuth } from 'app/models/user-auth.model';
import { environment } from 'environments/environment';
import { ErrorHandlingService } from 'app/services/error.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  registeredUserId = new Subject<string>();

  user = new Subject<UserAuth>();

  errorMsgValue = new Subject<string>();

  errorMsg!: string;

  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private errorService: ErrorHandlingService,
  ) {}

  signUp(email: string, password: string): Subscription {
    return this.httpClient
      .post<AuthResponseData>(`${environment.mongo.registerUrl}/register`, {
      email,
      password,
    })
      .pipe(
        catchError(this.errorService.logError),
        tap((resData) => {
          this.handleAuthentication(resData.email, resData.idToken);
        }),
      )
      .subscribe((resData) => {
        this.registeredUserId.next(resData.localId);
        this.router.navigate(['/login']);
      });
  }

  login(email: string, password: string): Subscription {
    return this.httpClient
      .post<AuthResponseData>(`${environment.mongo.registerUrl}/login`, {
      email,
      password,
    })
      .pipe(
        catchError(this.errorService.logError),
        tap((resData) => {
          this.handleAuthentication(resData.email, resData.idToken);
        }),
      )
      .subscribe();
  }

  logout(): void {
    this.user.next();
    sessionStorage.clear();
  }

  private handleAuthentication(email: string, token: string): void {
    const userAuthData = new UserAuth(email, token);
    this.user.next(userAuthData);
  }
}
