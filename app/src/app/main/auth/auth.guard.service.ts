import {
  CanActivate,
  Router,
  UrlTree,
} from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UsersService } from 'app/services/users.service';

@Injectable({
  providedIn: 'root',
})

export class AuthGuard implements CanActivate {
  constructor(
    private userServie: UsersService,
    private router: Router,
  ) { }

  canActivate(): boolean | Promise<boolean> | Observable<boolean> | UrlTree {
    const user = this.userServie.getUserFromStorage();

    return !!user || this.router.createUrlTree(['/login']);
  }
}
