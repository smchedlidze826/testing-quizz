import { Component, OnDestroy } from '@angular/core';
import { UsersService } from 'app/services/users.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnDestroy {
  name!: string;

  surname!: string;

  email!: string;

  password!: string;

  minLength: number = 6;

  subs!:Subscription;

  constructor(
    private userService: UsersService,
    private authService: AuthService,
  ) { }

  ngOnDestroy() {
    if (this.subs) {
      this.subs.unsubscribe();
    }
  }

  onSubmit(): void {
    const registerForm = {
      name: this.name,
      surname: this.surname,
      email: this.email,
      password: this.password,
    };

    this.authService.signUp(this.email, this.password);

    this.subs = this.authService.registeredUserId
      .subscribe(
        () => {
          this.userService.registerUserInfo({
            ...registerForm, status: 'user', passedTests: [], score: 0,
          });
        },
      );
  }
}
