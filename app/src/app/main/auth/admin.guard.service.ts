import {
  CanActivate,
  Router,
  UrlTree,
} from '@angular/router';
import { Injectable } from '@angular/core';
import { UsersService } from 'app/services/users.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})

export class AdminGuard implements CanActivate {
  constructor(
    private userService:UsersService,
    private router:Router,
  ) {}

  canActivate(): boolean | Promise<boolean> | Observable<boolean> | UrlTree {
    const isAdmin = this.userService.isAdmin();

    return isAdmin || this.router.createUrlTree(['/trainings']);
  }
}
