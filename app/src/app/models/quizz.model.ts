import { Question } from './questions.model';

export class Quizz {
  constructor(
    public author: string,
    public time: string,
    public numberOfQuestions: number,
    public questions: Question[] = [],
    public id: string = '',
    public title: string = '',
    public category: string = '',
    public difficulty: string = '',
    public description: string = '',
    public score?: number,
  ) {
  }
}
