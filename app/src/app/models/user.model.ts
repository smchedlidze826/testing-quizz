import { Quizz } from './quizz.model';

export class User {
  constructor(
    public name: string,
    public surname: string,
    public email: string,
    public status: string,
    public score: number,
    public passedTests: Quizz[],
    public dateOfBirth?: Date,
    public contactInfo?: string,
    public about?: string,
    public pic?:string,
    public _id?:string,
  ) {}
}
