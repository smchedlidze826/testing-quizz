import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})

export class ErrorHandlingService {
  logError(err: HttpErrorResponse) {
    alert(err.error.errMsg);
    return throwError(err.error.errMsg);
  }
}
