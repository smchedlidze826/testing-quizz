import { Injectable } from '@angular/core';
import { Quizz } from 'app/models/quizz.model';
import { User } from 'app/models/user.model';
import { UsersService } from 'app/services/users.service';

@Injectable({
  providedIn: 'root',
})

export class TestsService {
  constructor(private userService:UsersService) {}

  finalize(activeUser:User, quizz:Quizz, score:number, time:string): void {
    if (activeUser.passedTests) {
      activeUser.passedTests.push({ ...quizz, score, time } as Quizz);
    } else {
      activeUser.passedTests = [{ ...quizz, score, time } as Quizz];
    }

    const totalScore = activeUser.score + score;
    activeUser.score = totalScore;

    this.userService.update(activeUser);
    this.userService.updateDataInSessionStorage(activeUser);
  }
}
