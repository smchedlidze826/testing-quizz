import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Quizz } from 'app/models/quizz.model';
import { environment } from 'environments/environment';
import { Subject, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ErrorHandlingService } from './error.service';

export enum DialogMode { ADD, EDIT }

@UntilDestroy()
@Injectable({
  providedIn: 'root',
})

export class QuizzService {
  quizzStateChange = new Subject<Quizz | null>();

  quizzArrStateChange = new Subject<Quizz[]>();

  constructor(
    private httpClient: HttpClient,
    private errorService: ErrorHandlingService,
  ) { }

  delete(quizzId: string): Subscription {
    return this.httpClient.delete<{ quizz: Quizz }>(`${environment.mongo.databaseURL}/quizz/${quizzId}`)
      .pipe(
        untilDestroyed(this),
        catchError(this.errorService.logError),
      )
      .subscribe(
        () => this.getQuizz(),
      );
  }

  edit(updatedQuizz: Quizz): Subscription {
    return this.httpClient.put<{ quizz: Quizz }>(`${environment.mongo.databaseURL}/quizz/${updatedQuizz.id}`,
      updatedQuizz)
      .pipe(
        untilDestroyed(this),
        catchError(this.errorService.logError),
      )
      .subscribe(
        () => this.getQuizzById(updatedQuizz.id),
      );
  }

  add(quizz: Quizz): void {
    this.httpClient.post<Quizz>(`${environment.mongo.databaseURL}/quizz`, quizz)
      .pipe(
        untilDestroyed(this),
        catchError(this.errorService.logError),
      )
      .subscribe(
        () => this.getQuizz(),
      );
  }

  getQuizzById(id: string): Subscription {
    return this.httpClient.get<Quizz>(`${environment.mongo.databaseURL}/quizz/${id}`)
      .pipe(
        untilDestroyed(this),
        catchError(this.errorService.logError),
      )
      .subscribe(
        (quizzData) => this.quizzStateChange.next(quizzData),
      );
  }

  getQuizz(): Subscription {
    return this.httpClient.get<Quizz[]>(`${environment.mongo.databaseURL}/quizz`)
      .pipe(
        untilDestroyed(this),
        catchError(this.errorService.logError),
      )
      .subscribe(
        (quizzData) => this.quizzArrStateChange.next(quizzData),
      );
  }

  generateId(length: number): string {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i += 1) {
      result += characters.charAt(Math.floor(Math.random()
        * charactersLength));
    }
    return result;
  }

  create(author: string): Quizz {
    const minutes = '1';
    return new Quizz(author, minutes, 0);
  }
}
