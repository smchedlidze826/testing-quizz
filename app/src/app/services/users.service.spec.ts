import { HttpClient } from '@angular/common/http';
import { UsersService } from './users.service';

describe('users service', () => {
  let service: UsersService;
  let http: HttpClient;
  beforeEach(() => { service = new UsersService(http); });

  it('should return false', () => {
    const result = service.isAdmin();
    spyOn(service, 'isAdmin');
    expect(result).toBeFalse();
  });

  it('should return null', () => {
    const result = service.getUserFromStorage();
    spyOn(service, 'getUserFromStorage');
    expect(result).toBeNull();
  });
});
