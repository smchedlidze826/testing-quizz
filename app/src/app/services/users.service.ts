import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { User } from '../models/user.model';

@UntilDestroy()
@Injectable({
  providedIn: 'root',
})

export class UsersService {
  items!: Observable<User[]>;

  ADMIN_USER_Status: string = 'admin';

  authStateChange = new Subject<User>();

  usersArr = new Subject<User[]>();

  constructor(private http: HttpClient) { }

  update(activeUser: User): void {
    this.http.put<User>(`${environment.mongo.databaseURL}/users`, activeUser)
      .pipe(untilDestroyed(this))
      .subscribe();
  }

  registerUserInfo(userData: User): void {
    this.http.post<User>(`${environment.mongo.databaseURL}/users`, userData)
      .pipe(untilDestroyed(this))
      .subscribe();
  }

  getActiveUser(email: string): void {
    this.http.get<User>(`${environment.mongo.databaseURL}/users/${email}`)

      .pipe(untilDestroyed(this))
      .subscribe(
        (userData) => this.updateDataInSessionStorage(userData),
      );
  }

  getUsers(limit: number): void {
    this.http.get<User[]>(`${environment.mongo.databaseURL}/users?limit=${limit}`)
      .pipe(untilDestroyed(this))
      .subscribe(
        (usersData) => this.usersArr.next(usersData),
      );
  }

  filterUsers(filterCategory: string, filterValue: string, limit: number): void {
    this.http.get<User[]>(`${environment.mongo.databaseURL}/users/filter?category=${filterCategory}&value=${filterValue}&limit=${limit}`)
      .pipe(untilDestroyed(this))
      .subscribe(
        (userData) => {
          this.usersArr.next(userData);
        },
      );
  }

  isAdmin(): boolean {
    const activeUserData = this.getUserFromStorage();
    if (activeUserData) {
      return activeUserData.status === this.ADMIN_USER_Status;
    }
    return false;
  }

  getUserFromStorage(): User | null {
    const activeUserData = sessionStorage.getItem('activeUserData');
    if (activeUserData) {
      const parsedUser = JSON.parse(activeUserData);
      return parsedUser.user || parsedUser;
    }
    return null;
  }

  updateDataInSessionStorage(userData: User): void {
    sessionStorage.setItem('activeUserData', JSON.stringify(userData));
    this.authStateChange.next(userData);
  }
}
