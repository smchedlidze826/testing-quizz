import { NgModule } from '@angular/core';
import {
  RouterModule,
  Routes,
  PreloadAllModules,
} from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/trainings', pathMatch: 'full' },
  {
    path: 'login',
    loadChildren: () => import('./main/auth/auth.module').then(
      (m) => m.AuthModule,
    ),
  },
  {
    path: 'stats',
    loadChildren: () => import('./main/user-score-stats/stats.module').then(
      (m) => m.StatsModule,
    ),
  },
  {
    path: 'admin',
    loadChildren: () => import('./main/admin/admin.module').then(
      (m) => m.AdminModule,
    ),
  },
  {
    path: 'trainings',
    loadChildren: () => import('./main/trainings/trainings.module').then(
      (m) => m.TrainingsModule,
    ),
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes,
      {
        preloadingStrategy: PreloadAllModules,
      }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
