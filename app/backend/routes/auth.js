const router = require('express').Router();

const Auth = require('../models/auth');

const { authValidation } = require('../validation');

const jwt = require('jsonwebtoken');

const bcrypt = require('bcryptjs');

router.post('/register', async (req, res) => {

    const { error } = authValidation(req.body);
    if (error) return res.status(400).send({ errMsg: error.details[0].message });

    const emailExists = await Auth.findOne({ email: req.body.email });
    if (emailExists) return res.status(409).send({
        errMsg: 'User with this email is already registered'
    })

    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);

    const user = new Auth({
        email: req.body.email,
        password: hashedPassword
    })

    try {
        const savedUser = await user.save();
        const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);
        res.send(
            {
                email: req.body.email,
                idToken: token
            }
        )
    } catch (err) {
        res.status(500).send(err)
    }
})

router.post('/login', async (req, res) => {

    const { error } = authValidation(req.body);
    if (error) return res.status(400).send({ errMsg: error.details[0].message });

    const user = await Auth.findOne({ email: req.body.email });
    if (!user) return res.status(403).send({errMsg: 'User with this email does not exist'})

    const validPassword = await bcrypt.compare(req.body.password, user.password);
    if (!validPassword) return res.status(403).send({errMsg: 'Password is incorrect'})

    const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET)

    res.header('auth-token', token).send(
        {
            email: req.body.email,
            idToken: token
        }
    );

})

module.exports = router;