const express = require("express");

const router = express.Router();

const User = require('../models/user');

const { userDataValidation } = require('../validation');


router.use(express.urlencoded({ extended: false }));
router.use(express.json());


router.post("/api/users", (req, res, next) => {

  const { error } = userDataValidation(req.body);
  if (error) return res.status(400).send({ errMsg: error.details[0].message });

  const user = new User({
    name: req.body.name,
    surname: req.body.surname,
    email: req.body.email,
    status: req.body.status,
    score: req.body.score,
    passedTests: req.body.passedTests,
    dateOfBirth: req.body.dateOfBirth,
    contactInfo: req.body.contactInfo,
    about: req.body.about,
    pic: req.body.pic,
  });

  user.save();
  res.status(201).json({
    message: 'User added successfully',
    id: user.id
  });


});

router.get("/api/users/filter", (req, res, next) => {

  const value = req.query.value;
  const category = req.query.category
  let body;


  switch (category) {

    case 'surname':
      body = { 'surname': { $regex: value } };
      break;
    case 'status':
      body = { 'status': { $regex: value } };
      break;

    default:
      body = { 'name': { $regex: value } };
      break;
  }

  if (value) {
    User.find(body)
      .then(users => {
        res.status(200).json(users);
      })
  } else {
    getUserData(req, res);
  }
});

router.get("/api/users/:email", (req, res, next) => {
  const query = User.where({ email: req.params.email });
  query.findOne()
    .then(user => {
      res.status(200).json({
        user: user
      });
    })
    .catch(
      () => res.status(500).json(
        { errMsg: 'User with this email does not exists' }
      )
    )
});


router.get("/api/users", (req, res, next) => {

  getUserData(req, res);

});

router.put("/api/users", (req, res, next) => {
  const updatedUser = req.body;
  User.findOneAndUpdate({ _id: updatedUser._id }, updatedUser)
    .then(user => {
      res.status(200).json(user)
    })
    .catch(
      () => res.status(500).json(
        { errMsg: 'User with this id does not exists' }
      )
    )
});


function getUserData(req, res) {
  let size = req.query.limit;

  const limit = parseInt(size);

  User.find()
    .limit(limit)
    .then(users => {
      res.status(200).json(users);
    })
}


module.exports = router;
