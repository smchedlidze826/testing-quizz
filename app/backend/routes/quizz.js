const express = require("express");
const quizz = require("../models/quizz");

const router = express.Router();

const Quizz = require('../models/quizz');

router.use(express.urlencoded({ extended: false }));
router.use(express.json());

router.post("/api/quizz", (req, res, next) => {

  const quizz = new Quizz({
    title: req.body.title,
    category: req.body.category,
    difficulty: req.body.difficulty,
    author: req.body.author,
    description: req.body.description,
    time: req.body.time,
    numberOfQuestion: req.body.numberOfQuestion,
    questions: req.body.questions,
    score: req.body.score,
  });

  quizz.save();

  res.status(201).json({
    message: 'quizz added successfully',
    id: quizz.id
  });

});


router.get("/api/quizz", (req, res, next) => {

  Quizz.find()
    .then(
      (quizzArr) => {
        let newQuizzArr = [];
        quizzArr.forEach(quizz => {
          const newQuizz = getQuizzModel(quizz);
          newQuizzArr.push(newQuizz);
        })
        return newQuizzArr
      }
    )
    .then(quizz => {
      res.status(200).json(quizz);
    })
    .catch(() => res.status(500).send({ errMsg: 'Something went wrong' }))

});

router.get("/api/quizz/:id", (req, res, next) => {

  const query = Quizz.where({ _id: req.params.id });

  query.findOne()
    .then(quizz => {
      const transformedQuizz = getQuizzModel(quizz);
      return transformedQuizz;
    })
    .then(quizz => {
      res.status(200).json(quizz);
    })
    .catch(() => sendErr(res))

});

router.put("/api/quizz/:id", (req, res, next) => {

  const id = req.params.id;
  const updatedQuizz = req.body;

  Quizz.findByIdAndUpdate(id, updatedQuizz, {
    new: true
  })
  .catch(() => sendErr(res))

});

router.delete("/api/quizz/:id", (req, res, next) => {

  const id = req.params.id;

  Quizz.findByIdAndDelete(id).then(

    Quizz.find()
      .then(quizz => {
        res.status(200).json({
          quizz: quizz
        });
      })
      .catch(() => sendErr(res))
  )

});


function sendErr(res) {
  return res.status(404).json({
    errMsg: 'Quizz with that id was not found'
  })

}

function getQuizzModel(quizz) {

  return {
    id: quizz._id,
    title: quizz.title,
    category: quizz.category,
    difficulty: quizz.difficulty,
    author: quizz.author,
    description: quizz.description,
    time: quizz.time,
    numberOfQuestions: quizz.numberOfQuestions,
    questions: quizz.questions,
    score: quizz.score,
  }

}

module.exports = router;
