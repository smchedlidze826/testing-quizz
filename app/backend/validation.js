const Joi = require('joi');


const authValidation = (data) => {

    const MIN_lENGTH = 6;
    const MAX_lENGTH = 20;


    const schema = Joi.object({
        email: Joi.string().email().min(MIN_lENGTH).max(MAX_lENGTH).required(),
        password: Joi.string().min(MIN_lENGTH).max(MAX_lENGTH).required()
    })

    return schema.validate(data)
}

const userDataValidation = (data) => {

    const MIN_lENGTH_NAME_SURNAME = 2;
    const MIN_lENGTH_EMAIL = 6;
    const MAX_lENGTH = 20;
    const MIN_SCORE = 0;

    const schema = Joi.object({
        name: Joi.string().min(MIN_lENGTH_NAME_SURNAME).max(MAX_lENGTH).required(),
        surname: Joi.string().min(MIN_lENGTH_NAME_SURNAME).max(MAX_lENGTH).required(),
        email: Joi.string().email().min(MIN_lENGTH_EMAIL).max(MAX_lENGTH).required(),
        status: Joi.string().required(),
        score: Joi.number().min(MIN_SCORE).required(),
    })

    return schema.validate(data)

}

module.exports.authValidation = authValidation;

module.exports.userDataValidation = userDataValidation;
