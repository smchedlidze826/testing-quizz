const mongoose = require('mongoose');

const authSchema = mongoose.Schema({
    email: { type: String, required: true,min:6, max: 50 },
    password: { type: String, required: true,min:6, max: 50 },
});

module.exports = mongoose.model('Auth', authSchema);