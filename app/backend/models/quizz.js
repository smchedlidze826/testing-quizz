const mongoose = require('mongoose');


const quizzSchema = mongoose.Schema({
    title: { type: String, required: true },
    category: { type: String, required: true },
    difficulty: { type: String, required: true },
    author: { type: String, required: true },
    description: { type: String, required: true },
    time: { type: String, required: true },
    numberOfQuestions: { type: Number, required: false },
    questions: { type: Array, required: false },
    score: { type: Number, required: false },
});

module.exports = mongoose.model('Quizz', quizzSchema);