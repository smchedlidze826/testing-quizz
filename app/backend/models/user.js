const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    name: {type:String, required:true},
    surname: {type:String, required:true},
    email: {type:String, required:true},
    status: {type:String, required:true},
    score: {type:Number, required:true},
    passedTests: {type:Array, required:true},
    dateOfBirth: {type:Date, required:false},
    contactInfo: {type:String, required:false},
    about: {type:String, required:false},
    pic: {type:String, required:false}
});

module.exports = mongoose.model('User', userSchema);