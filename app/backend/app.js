const express = require("express");

const app = express();

const mongoose = require('mongoose');

const dotenv = require('dotenv');

dotenv.config();
mongoose.connect(process.env.DB_CONNECT);

const userRoutes = require('./routes/user');
const quizzRoutes = require('./routes/quizz');
const authRoutes = require('./routes/auth');



app.use(express.urlencoded({ extended: false }));
app.use(express.json());


app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, DELETE, OPTIONS, PUT"
  );
  next();
});


app.use(userRoutes);
app.use(quizzRoutes);
app.use(authRoutes);



module.exports = app;
